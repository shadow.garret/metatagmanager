#include "tileviewwidget.h"
#include <QDateTime>
#include <QDebug>
#include <QRect>

    TileViewWidget::TileViewWidget(QWidget *parent) : QAbstractItemView(parent) // QWidget(parent)
    {
        m_ItemsPerRow = 5;
        UpdateLayout();
        this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
        this->sizePolicy().setHeightForWidth(true);
        this->setSelectionMode(QAbstractItemView::ExtendedSelection);
        this->setSelectionBehavior(QAbstractItemView::SelectItems);
        this->verticalScrollBar()->setSingleStep(10);
        this->horizontalScrollBar()->setSingleStep(10);
        //this->setAttribute(Qt::WA_OpaquePaintEvent);
    }

    void TileViewWidget::contextMenuEvent(QContextMenuEvent *event)
    {
        emit ContextMenuEvent(event);
    }

    QList<int> TileViewWidget::GetVisibleIndices(QRect rect)
    {
        QSize totalTileSize = ((ImageEntityModel*)this->model())->TotalTileSize();
        int line = (int)rect.y() / (totalTileSize.height()+1);
        int row = (int)rect.x() / (totalTileSize.width()+1);

        int lines = ((int)(rect.y() + rect.height() - 1) / (totalTileSize.height()+1)) - ((int)rect.y() / (totalTileSize.height()+1)) + 1;
        //int rows = ((int)(rect.x() + rect.width() - 1) / (totalTileSize.width()+1)) - ((int)rect.x() / (totalTileSize.width()+1)) + 1;

        int rows = (int)rect.width() / (totalTileSize.width()+1);
        if (lines == 0) lines = 1; // Draw at last one item in visible area.
        if (rows == 0) rows = 1;
        //qDebug()<<"GetVisibleIndices"<<rect<<"line"<<line<<"lines"<<lines<<"row"<<row<<"rows"<<rows;

        QList<int> indices = QList<int>();
        int i;
        for (int l = line; l < line+lines; l++)
        {
            for (int r = row; r < row+rows; r++)
            {
                i = l * m_ItemsPerRow + r;
                if (i >= model()->rowCount(rootIndex())) break;//m_Items
                if (indices.contains(i)) continue;
                indices.append(i);
            }
        }
        return indices;
    }

    void TileViewWidget::setModel(QAbstractItemModel *model)
    {
        QAbstractItemView::setModel(model);
    }

    /// Should be called on any change to TileMargins, TilePaddings, TileSize
    void TileViewWidget::UpdateLayout()
    {
        if (model() == NULL) return;
        QSize totalTileSize = ((ImageEntityModel*)this->model())->TotalTileSize();
        /*m_TotalTileSize.setWidth(m_TileMargins.left() + m_TilePaddings.left() + m_TileSize.width() + m_TilePaddings.right() + m_TileMargins.right());
        m_TotalTileSize.setHeight(m_TileMargins.top() + m_TilePaddings.top() + m_TileSize.height() + m_TilePaddings.bottom() + m_TileMargins.bottom());
        */
        int new_ItemsPerRow = viewport()->width() / (totalTileSize.width()+1);
        if (new_ItemsPerRow<=0) new_ItemsPerRow = 1;
        bool scroll = m_ItemsPerRow == new_ItemsPerRow;
        m_ItemsPerRow = new_ItemsPerRow;


        //scrollbar
        horizontalScrollBar()->setPageStep(viewport()->width()/2);
        horizontalScrollBar()->setRange(0, qMax(0, totalTileSize.width() - viewport()->width()));
        horizontalScrollBar()->setSingleStep(totalTileSize.width()/10);
        //verticalScrollBar()->setSingleStep(RowHeight);
        verticalScrollBar()->setPageStep(viewport()->height()/2);
        verticalScrollBar()->setRange(0, qMax(0, (totalTileSize.height()+1)*(int)ceil((double)model()->rowCount(rootIndex())/m_ItemsPerRow) - viewport()->height()));
        verticalScrollBar()->setSingleStep(totalTileSize.height()/10);
        //qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"UpdateLayout:"<<m_TotalTileSize.width()<<m_TotalTileSize.height() <<"ItemsPerRow:"<<m_ItemsPerRow;
        if (scroll) this->scrollTo(currentIndex(), ScrollHint::EnsureVisible);
    }

    QPoint TileViewWidget::GetItemLocation(const int index) const
    {
        QSize totalTileSize = ((ImageEntityModel*)this->model())->TotalTileSize();
        return QPoint(index % m_ItemsPerRow * (totalTileSize.width()+1), index / m_ItemsPerRow * (totalTileSize.height()+1));
    }

    int TileViewWidget::GetIndexAtLocation(const QPoint point) const
    {
        QSize totalTileSize = ((ImageEntityModel*)this->model())->TotalTileSize();
        int line = (point.y()-3) / (totalTileSize.height()+1);        // 3 = visual mouse pointer offset
        int row = (point.x()-2) / (double)(totalTileSize.width()+1);  // 2 = visual mouse pointer offset
        //qDebug()<<"IndexAt("<<point<<"): "<<line<<row;
        if (ceil((point.x()-2)/(double) (totalTileSize.width()+1)) > m_ItemsPerRow) return -1;

        int r = (line * m_ItemsPerRow + row);
        //qDebug()<< "IndexAtLocation:"<<r<<"line:"<<line<<"row:"<<row;
        if (r >= model()->rowCount(rootIndex())) return -1; //m_Items
        else return r;
    }

    void TileViewWidget::paintEvent(QPaintEvent *event)
    {
        ImageEntityModel* model = (ImageEntityModel*)this->model();
        QSize totalTileSize = model->TotalTileSize();
        QPainter painter(viewport());

        //qDebug()<<"paintEvent viewport"<<viewport()->rect() << "" << viewport()->pos() <<"event"<<event->rect();
        QRect rectangle = event->rect().translated(horizontalScrollBar()->value(),
                    verticalScrollBar()->value()).normalized();
        QRect rectangle2 = viewport()->rect().translated(horizontalScrollBar()->value(),verticalScrollBar()->value()).normalized();
        //painter.setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);
        //if (model() == NULL) return;
        QList<int> indices = GetVisibleIndices(rectangle); //event->rect()
        QList<int> allIndices = GetVisibleIndices(rectangle2);
        //qDebug()<<rectangle<<indices<<rectangle2<<allIndices;

        if (allIndices.count()>0)
        {
            int count = model->cachedIndices->count();

            if (count == 0 || count < allIndices.count() || (model->cachedIndices->at(0) > allIndices[0] || model->cachedIndices->at(count - 1) < allIndices[allIndices.count() - 1]))
                model->cacheThumbnails(allIndices);
        }
        //for (int row = 0; row < model()->rowCount(rootIndex()); ++row) {
        QList<int>::iterator row;
        for (row = indices.begin(); row != indices.end(); ++row) {
            QModelIndex index = model->index(*row, 0, rootIndex());
            QPoint point = GetItemLocation(index.row());
            QRectF rect = QRectF(point.rx() - horizontalScrollBar()->value(),point.ry() - verticalScrollBar()->value(), totalTileSize.width(), totalTileSize.height()); //viewportRectForRow(row);
            if (!rect.isValid() || rect.bottom() < 0 ||
                rect.y() > viewport()->height())
                continue;
            QStyleOptionViewItem option = viewOptions();
            option.rect = rect.toRect();
            if (selectionModel()->isSelected(index))
                option.state |= QStyle::State_Selected;
            if (currentIndex() == index)
                option.state |= QStyle::State_HasFocus;

            itemDelegate()->paint(&painter, option, index);
            //paintOutline(&painter, rect);
        }
        //painter.setPen(Qt::red);
        //painter.drawRect(QRect(event->rect().topLeft(), QPoint(event->rect().right()-1,event->rect().bottom()-1)));
        /*
        QPainter painter(this);
        //qDebug() << QString("[%1]").arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz")) << ", rect: " << rect() << ", clipRegion: " << painter.clipRegion() << ", clipBoundingRect: " << painter.clipBoundingRect();

        //qDebug() << QString("[%1]").arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz")) <<event->rect();
        QList<int> indices = GetVisibleIndices(event->rect());
        //qDebug() << this->visibleRegion().boundingRect();

        //painter.fillRect(this->rect(),QBrush(Qt::white, Qt::SolidPattern));

        QList<int>::iterator i;
        for (i = indices.begin(); i != indices.end(); ++i)
        {
             QPoint basepoint = GetItemLocation(*i);
             painter.setPen(Qt::red);
             QRect totalrect(basepoint, m_TotalTileSize);
             painter.drawRect(totalrect);

             painter.setPen(Qt::black);
             QRect tilerect(QPoint(basepoint.x()+m_TileMargins.left(), basepoint.y()+m_TileMargins.top()), QSize(m_TilePaddings.left()+m_TileSize.width()+m_TilePaddings.right(),m_TilePaddings.top()+m_TileSize.height()+m_TilePaddings.bottom()));
             painter.drawRect(tilerect);

             QRect contentrect(QPoint(basepoint.x()+m_TileMargins.left()+m_TilePaddings.left(), basepoint.y()+m_TileMargins.top()+m_TilePaddings.top()), m_TileSize);
             painter.fillRect(contentrect, QBrush(Qt::cyan, Qt::SolidPattern));

             QRect textrect(QPoint(basepoint.x()+m_TileMargins.left(),basepoint.y()+m_TileMargins.top()+m_TilePaddings.top()+m_TileSize.height()-1), QSize(tilerect.width(),tilerect.height()-contentrect.height()));
             painter.drawText(textrect, QString("%1").arg(*i), textOption);
             //painter.drawText(QPoint(tilerect.center().x(),tilerect.bottom()), QString("%1").arg(*i));

        }*/
    }

    void TileViewWidget::resizeEvent(QResizeEvent *) {UpdateLayout();}

    /*void TileViewWidget::mousePressEvent (QMouseEvent *event)
    {
        int idx = GetIndexAtLocation(event->pos());

        qDebug()<<"mousePressEvent,"<<"idx:"<<idx<<", pos:"<<event->pos()<<", button:"<<event->button();
        QAbstractItemView::mousePressEvent(event);
    }*/

    QSize TileViewWidget::sizeHint() const
    {
        QSize size;
        size = QSize(this->parentWidget()->width(), heightForWidth(this->parentWidget()->width()));
        //qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"sizeHint:"<<size<<"(from parent)";
        return size;
        //return minimumSizeHint();
    }

    QSize TileViewWidget::minimumSizeHint() const
    {
        //QSize size;
//        if (this->parentWidget()->width()<=m_TotalTileSize.width()+1)
//        {
//            size = QSize(m_TotalTileSize.width()+1, heightForWidth(m_TotalTileSize.width()+1));
//            //qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"minimumSizeHint:"<<size<<"(calculated)";
//        }
//        else
//        {
//            size = QSize(this->parentWidget()->width(),heightForWidth(this->parentWidget()->width()));
//        //    qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"minimumSizeHint:"<<size<<"(from parent)";
//        }
        //return size;
        if (model()==NULL) return QSize(270,297);
        return ((ImageEntityModel*)this->model())->TotalTileSize();
    }

    int TileViewWidget::heightForWidth(int width) const
    {
        if (model() == NULL)
            return ceil(0/(double)(width/(270+1)))*(297+1);
        QSize totalTileSize = ((ImageEntityModel*)this->model())->TotalTileSize();
        int result = ceil(this->model()->rowCount(rootIndex())/(double)(width/(totalTileSize.width()+1)))*(totalTileSize.height()+1);
        //qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"heightForWidth"<<width<<"result in"<<result;
        return result;
    }

    //QAbstractItemView



    QRect TileViewWidget::visualRect(const QModelIndex &index) const
    {
        /*QRect rect;
        if (index.isValid())
        {
            //calculateRectsIfNecessary();
            rect = rectForRow.value(index.row()).toRect();
            if (!rect.isValid())
                return rect;
            rect = QRectF(rect.x() - horizontalScrollBar()->value(),
                          rect.y() - verticalScrollBar()->value(),
                          rect.width(), rect.height()).toRect();
        }
        return rect;*/
        QRect rect;
        if (index.isValid())
        {
            rect = QRect(GetItemLocation(index.row()),((ImageEntityModel*)this->model())->TotalTileSize()+QSize(1,1));
            rect.translate(-horizontalScrollBar()->value(), -verticalScrollBar()->value());
            //rect = QRect(rect.x() - horizontalScrollBar()->value(), rect.y() - verticalScrollBar()->value(), rect.width(), rect.height());
        }
        //qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"visualRect"<<index.row()<<rect;
        return rect;
    }

    void TileViewWidget::scrollTo(const QModelIndex &index, QAbstractItemView::ScrollHint /*hint*/)
    {
        //QRect viewRect = viewport()->rect();
        QRect viewRect = viewport()->rect().translated(horizontalScrollBar()->value(),
                    verticalScrollBar()->value()).normalized();
        QRect itemRect = visualRect(index).translated(horizontalScrollBar()->value(),
                                                      verticalScrollBar()->value()).normalized();
        int hsb = horizontalScrollBar()->value();
        int vsb = verticalScrollBar()->value();
        int new_hsb=hsb,new_vsb=vsb;
        if (itemRect.left() < viewRect.left())
            new_hsb = horizontalScrollBar()->value() + itemRect.left() - viewRect.left();
        else if (itemRect.right() > viewRect.right())
            new_hsb = horizontalScrollBar()->value() + qMin(itemRect.right() - viewRect.right(), itemRect.left() - viewRect.left());
        if (itemRect.top() < viewRect.top())
            new_vsb = verticalScrollBar()->value() + itemRect.top() - viewRect.top();
        else if (itemRect.bottom() > viewRect.bottom())
            new_vsb = verticalScrollBar()->value() + qMin(itemRect.bottom() - viewRect.bottom(), itemRect.top() - viewRect.top());
        if (hsb != new_hsb || vsb != new_vsb)
        {
            horizontalScrollBar()->setValue(new_hsb);
            verticalScrollBar()->setValue(new_vsb);
            //qDebug()<<"scrollTo"<<index.row()<<"update"<<hsb<<new_hsb;
            viewport()->update();
        }
        //else qDebug()<<"scrollTo"<<index.row()<<"no update"<<hsb<<new_hsb;
    }

    QModelIndex TileViewWidget::indexAt(const QPoint &point) const
    {
        /*QPoint p(point);
        p.rx() += horizontalScrollBar()->value();
        p.ry() += verticalScrollBar()->value();
        //calculateRectsIfNecessary();
        QHashIterator<int, QRectF> i(rectForRow);
        while (i.hasNext()) {
            i.next();
            if (i.value().contains(p))
                return model()->index(i.key(), 0, rootIndex());
        }*/
        //.translated(horizontalScrollBar()->value(), verticalScrollBar()->value()).normalized();
        int index = GetIndexAtLocation(QPoint(point.x()+horizontalScrollBar()->value(),point.y()+verticalScrollBar()->value()));
        if (index>=0)
            return model()->index(index,0, rootIndex());
        else
            return QModelIndex();
    }

    QModelIndex TileViewWidget::moveCursor(QAbstractItemView::CursorAction cursorAction, Qt::KeyboardModifiers /*modifiers*/)
    {
        QModelIndex index = currentIndex();
        if (index.isValid()) {
            /*if ((cursorAction == MoveLeft && index.row() > 0) ||
                (cursorAction == MoveRight &&
                 index.row() + 1 < model()->rowCount())) {
                const int offset = (cursorAction == MoveLeft ? -1 : 1);
                index = model()->index(index.row() + offset,
                                       index.column(), index.parent());
            }
            else if ((cursorAction == MoveUp && index.row() > 0) ||
                     (cursorAction == MoveDown &&
                      index.row() + 1 < model()->rowCount())) {
                QFontMetrics fm(font());
                const int RowHeight = (fm.height() + ExtraHeight) *
                                      (cursorAction == MoveUp ? -1 : 1);
                QRect rect = viewportRectForRow(index.row()).toRect();
                QPoint point(rect.center().x(),
                             rect.center().y() + RowHeight);
                while (point.x() >= 0) {
                    index = indexAt(point);
                    if (index.isValid())
                        break;
                    point.rx() -= fm.width("n");
                }
            }*/
            int newindex = index.row();
            if (cursorAction == MoveLeft)
                --newindex;
            if (cursorAction == MoveRight)
                ++newindex;
            if (cursorAction == MoveUp)
                newindex-=m_ItemsPerRow;
            if (cursorAction == MoveDown)
                newindex+=m_ItemsPerRow;
            if (newindex < 0)
                newindex = 0;
            if (newindex >
                    model()->rowCount())
                newindex =
                        model()->rowCount();
            index = model()->index(newindex, 0, rootIndex());
            scrollTo(index, ScrollHint::EnsureVisible);
        }
        return index;
    }

    int TileViewWidget::horizontalOffset() const
    {
        return horizontalScrollBar()->value();
    }

    int TileViewWidget::verticalOffset() const
    {
        return verticalScrollBar()->value();
    }

    void TileViewWidget::setSelection(const QRect &rect, QItemSelectionModel::SelectionFlags flags)
    {
        QRect rectangle = rect.translated(horizontalScrollBar()->value(),
                    verticalScrollBar()->value()).normalized();


            int firstRow = model()->rowCount();
            int lastRow = -1;

            firstRow = GetIndexAtLocation(rectangle.topLeft());
            lastRow = GetIndexAtLocation(rectangle.bottomRight());

            //qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"setSelection"<<rectangle<<"firstRow:"<<firstRow<<"lastRow:"<<lastRow;

            if (firstRow != model()->rowCount() && lastRow != -1) {
                QItemSelection selection(
                        model()->index(firstRow, 0, rootIndex()),
                        model()->index(lastRow, 0, rootIndex()));
                selectionModel()->select(selection, flags);

                ((ImageEntityModel*)this->model())->setSelectedItems(selection.indexes());
            }
            else {
                qDebug()<<"setSeelction: invalid selection";
                QModelIndex invalid;
                QItemSelection selection(invalid, invalid);
                selectionModel()->select(selection, flags);
            }
            scrollTo(model()->index(lastRow, 0, rootIndex()), ScrollHint::EnsureVisible);
    }

    QRegion TileViewWidget::visualRegionForSelection(const QItemSelection &selection) const
    {
        QRegion region;
        QList<int> rows;
        foreach (const QItemSelectionRange &range, selection) {
            for (int row = range.top(); row <= range.bottom(); ++row) {
                rows.append(row);
                /*for (int column = range.left(); column < range.right();
                     ++column) {*/
                    QModelIndex index = model()->index(row, /*column*/0, rootIndex());
                    region += visualRect(index);
                //}
            }
        }
        //qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"selection region"<<rows;
        return region;
    }

    void TileViewWidget::rowsInserted(const QModelIndex &parent, int start, int end)
    {
        UpdateLayout();
        QAbstractItemView::rowsInserted(parent, start, end);
    }

    void TileViewWidget::rowsAboutToBeRemoved(const QModelIndex &parent, int start, int end)
    {
        UpdateLayout();
        QAbstractItemView::rowsAboutToBeRemoved(parent, start, end);
    }

    void TileViewWidget::currentChanged(const QModelIndex &current, const QModelIndex &previous)
    {
        emit CurrentChanged(current, previous);
    }

    TileViewWidget::~TileViewWidget()
    {

    }
