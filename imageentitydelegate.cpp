#include "imageentitydelegate.h"
#ifdef QT_DEBUG
#include <QDebug>
#include <QDateTime>
#endif
#include "imageentity.h"
#include "imageentitymodel.h"
#include "io.h"

ImageEntityDelegate::ImageEntityDelegate(QObject *parent) :
    QAbstractItemDelegate(parent)
{
}

void ImageEntityDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    ImageEntity entity = index.data().value<ImageEntity>();
    ImageEntityModel *model = (ImageEntityModel*)index.model();
    QPoint basepoint = option.rect.topLeft();//GetItemLocation(index.row());
    QMargins tileMargins = model->TileMargins(),
            tilePaddings = model->TilePaddings();
    QSize tileSize = model->TileSize()/*,
            totalTileSize = model->TotalTileSize()*/;

    painter->save();
    if (option.state & QStyle::State_Selected)
    {
        painter->fillRect(QRect(option.rect.topLeft(), option.rect.size()+QSize(1,1)), QColor(0,0,255,128));
    }
    //QRect totalrect(basepoint, m_TotalTileSize);
    //painter->setPen(Qt::red);
    //painter->drawRect(option.rect); //totalrect

    painter->setPen(Qt::black);

    QRect tilerect(QPoint(basepoint.x()+tileMargins.left(), basepoint.y()+tileMargins.top()),
                   QSize(tilePaddings.left()+tileSize.width()+tilePaddings.right(),
                         tilePaddings.top()+tileSize.height()+tilePaddings.bottom())
                   );
    painter->drawRect(tilerect);


    QRect contentrect(QPoint(tileMargins.left()+tilePaddings.left()+basepoint.x(), tileMargins.top()+tilePaddings.top()+basepoint.y()), tileSize);

    std::shared_ptr<QImage> thumbnail = model->getThumbnail(index); //entity.Thumbnail();
    if (thumbnail != NULL && !thumbnail->isNull())
    {
        QPoint thumbnailPos(contentrect.x()+(contentrect.width()-thumbnail->width())/2,contentrect.y()+(contentrect.height()-thumbnail->height())/2);
        painter->drawImage(thumbnailPos, *thumbnail);
    }
    else
        painter->fillRect(contentrect, QBrush(Qt::cyan, Qt::SolidPattern));

    QRect textrect(QPoint(basepoint.x()+tileMargins.left(),basepoint.y()+tileMargins.top()+tilePaddings.top()-2+tileSize.height()-1), QSize(tilerect.width(),tilerect.height()-contentrect.height()));
    painter->drawText(textrect, entity.Filename(), model->TextOption());
    painter->restore();
    //painter.drawText(QPoint(tilerect.center().x(),tilerect.bottom()), QString("%1").arg(*i));
}

QSize ImageEntityDelegate::sizeHint(const QStyleOptionViewItem &/*option*/, const QModelIndex &index) const
{
    ImageEntityModel *model = (ImageEntityModel*)index.model();
    return model->TotalTileSize();
    //return m_TotalTileSize;
}

