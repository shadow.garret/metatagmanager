#include "io.h"

/*
 * Xmp.pdf.Author       XmpText
 * Xmp.pdf.Title        XmpText
 * Xmp.dc.source        XmpText
 * Xmp.dc.description   LangAlt
 * Xmp.dc.subject       XmpBag      Keywords
 */

/*IO::IO()
{
}*/

QSize size = QSize(256,256);

ImageEntity* IO::Meta::ReadMetaFromFile(ImageEntity * entity)
{
    try{
        Exiv2::Image::AutoPtr img = Exiv2::ImageFactory::open(QDir::current().filePath(entity->Filename()).toStdString());

        if (img->good())
        {
            img->readMetadata();
            entity->setSize(QSize(img->pixelWidth(),img->pixelHeight()));

            Exiv2::XmpData &xmpData = img->xmpData();

            if (xmpData["Xmp.pdf.Title"].count()) entity->setTitle(QString::fromStdString(xmpData["Xmp.pdf.Title"].value().toString()));
            if (xmpData["Xmp.pdf.Author"].count()) entity->setAuthor(QString::fromStdString(xmpData["Xmp.pdf.Author"].value().toString()));
            if (xmpData["Xmp.dc.subject"].count()) {
                if (xmpData["Xmp.dc.subject"].typeId()== Exiv2::xmpBag)
                {
                    for(int i=0; i<xmpData["Xmp.dc.subject"].count(); i++)
                        entity->Keywords().append(QString::fromStdString(xmpData["Xmp.dc.subject"].value().toString(i)));
                }
                else
                    entity->Keywords().append(QString::fromStdString(xmpData["Xmp.dc.subject"].value().toString()));
            }
            if (xmpData["Xmp.dc.source"].count()) entity->setSource(QString::fromStdString(xmpData["Xmp.dc.source"].value().toString()));
            if (xmpData["Xmp.dc.description"].count()) entity->setDescription(QString::fromStdString(xmpData["Xmp.dc.description"].value().toString(0)));

            QFileInfo *file = new QFileInfo(entity->Filename());
            entity->setModified(file->lastModified());
            delete file;

            /*
        Exiv2::XmpData::iterator xend = xmpData.end();
        for (Exiv2::XmpData::iterator mxd = xmpData.begin(); mxd != xend; ++mxd) {
            //meta->insert(QString::fromStdString(mxd->key()), QString::fromStdString(mxd->value().toString()));
            std::cout << std::setw(44) << std::setfill(' ') << std::left
                      << mxd->key() << " "
                      << "0x" << std::setw(4) << std::setfill('0') << std::right
                      << std::hex << mxd->tag() << " "
                      << std::setw(9) << std::setfill(' ') << std::left
                      << mxd->typeName() << " "
                      << std::setw(9) << std::setfill(' ') << std::left
                      << mxd->typeId() << " "
                      << std::dec << std::setw(3)
                      << std::setfill(' ') << std::right
                      << mxd->count() << "  "
                      << std::dec << mxd->value()
                      << std::endl;
        }*/
        }
        else
            qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"ReadMeta "<<entity->Filename()<<": Invalid file.";
        img->io().close();
    }
    catch (Exiv2::AnyError &error)
    {
        qDebug()<<entity->Filename()<<error.what();
    }

    return entity;
}

std::shared_ptr<QImage> IO::Thumbnails::CreateThumbnail(ImageEntity* entity)
{
    QImage image;
    if (entity->Filename() == "")
    {
        qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"CreateThumbnail: entity filename is empty";
        return std::make_shared<QImage>(image);
    }

    image.load(entity->Filename());
    std::shared_ptr<QImage> scaled = std::make_shared<QImage>(image.scaled(size, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    entity->setSize(image.size());
    //qDebug()<<"CreateThumbnail"<<image.size()<<scaled->size();
    return scaled;
}

std::shared_ptr<QFile> IO::Thumbnails::filePtr = std::shared_ptr<QFile>(new QFile(QDir::currentPath()+"/thumbnails_.bin"));
void IO::Thumbnails::setFile(ImageEntityModel &model)
{
    filePtr = std::shared_ptr<QFile>(new QFile(QDir::currentPath()+"/thumbnails_.bin"));

    bool newFile = !filePtr->exists();
    filePtr->open(QFile::ReadWrite);

    QDataStream stream(filePtr.get());
    if (newFile)
    {
        stream<<0x4D544D47; //MTMG
        filePtr.get()->flush();
    }
    else
    {
        quint32 startstring;
        stream>>startstring;
        if (startstring != 0x4D544D47)
        {
            qDebug()<<"setFile wrong startstring"<<startstring<<"should be"<<0x4D544D47;
            return;
        }
        while(!stream.device()->atEnd())
        {
            quint16 start,end;
            QString filename;
            quint32 thumbLength;
            //QImage thumbnail;
            stream>>start;
            stream>>filename;
            quint64 pos = filePtr.get()->pos();
            stream>>thumbLength;
            //stream>>thumbnail; //skip thumbnails image
            filePtr.get()->seek(pos+4+thumbLength);
            stream>>end;
            ImageEntity* entity = model.findItemByName(filename);
            //qDebug()<<"Thumbnails::setPath"<<entity->Filename();
            entity->setThumbnailPos(pos);
            //model.setData(model.index(model.indexOf(entity)), QVariant::fromValue<ImageEntity>(entity),Qt::EditRole);
            //model.setThumbnail(model.index(model.indexOf(entity)), thumbnail);
        }
    }
    //if (!file->open(QFile::ReadWrite))
    //    qDebug()<<QDateTime::currentDateTime().toString("[hh:mm:ss.zzz]")<<"IO::Thumbnails::init: Cannot open file.";

}

QMutex fileMutex;
std::shared_ptr<QImage> IO::Thumbnails::ReadThumbnail(ImageEntity& entity)
{
    std::shared_ptr<QImage> img = std::make_shared<QImage>();
    if (entity.ThumbnailPos()==0){
        qDebug()<<"ReadThumbnail"<<"ThumbnailPos eq 0";
        return img;
    }
    //qDebug()<<"fileMutex locked";
    fileMutex.lock();
    filePtr->seek(entity.ThumbnailPos());
    QDataStream stream(filePtr.get());
    QByteArray ba;
    stream>>ba;
    fileMutex.unlock();
    //qDebug()<<"fileMutex unlocked";
    img->loadFromData(ba);
    return img;
}

void IO::Thumbnails::WriteThumbnail(ImageEntity* entity, std::shared_ptr<QImage> img)
{
    //qDebug()<<"fileMutex locked";
    fileMutex.lock();
    QDataStream stream(filePtr.get());
    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QBuffer::WriteOnly);
    img->save(&buffer,"PNG");

    if (entity->ThumbnailPos() != 0)
    {
        qDebug()<<"WriteThumbnail"<<"existing";
        filePtr->seek(entity->ThumbnailPos());
        stream<<ba;
        filePtr->seek(filePtr->size());
    }
    else
    {
        //qDebug()<<"WriteThumbnail"<<"new";
        if (filePtr->pos()!=filePtr->size())
            filePtr->seek(filePtr->size());
        stream<<(quint16)0xF00F;
        stream<<entity->Filename();
        entity->setThumbnailPos(filePtr->pos());
        stream<<ba;
        stream<<(quint16)0xF00F;
    }
    fileMutex.unlock();
    //qDebug()<<"fileMutex unlocked";
}

IO::Thumbnails::~Thumbnails()
{
    if (filePtr->isOpen()) filePtr->close();
}
