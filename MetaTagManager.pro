#-------------------------------------------------
#
# Project created by QtCreator 2014-02-18T14:38:57
#
#-------------------------------------------------

QT       += core gui
QT       += concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES += __GXX_EXPERIMENTAL_CXX0X__
greaterThan(QT_MAJOR_VERSION, 4): CONFIG += c++11
lessThan(QT_MAJOR_VERSION, 5): CONFIG += -std=c++11

TARGET = MetaTagManager
TEMPLATE = app

SOURCES += main.cpp\
        tileviewwidget.cpp \
    mainwindow.cpp \
    io.cpp \
    infopanelwidget.cpp \
    imageentitymodel.cpp \
    imageentitydelegate.cpp \
    imageentity.cpp

HEADERS  += tileviewwidget.h \
    mainwindow.h \
    io.h \
    infopanelwidget.h \
    imageentitymodel.h \
    imageentitydelegate.h \
    imageentity.h

FORMS    += \
    mainwindow.ui \
    infopanel.ui

#CONFIG += static

macx {
    LIBS += -L/usr/local/lib/ -lexiv2
    INCLUDEPATH += /usr/local/include
    DEPENDPATH += /usr/local/include
}

win32 {
    CONFIG += windows

    contains(QMAKE_TARGET.arch, x86_64){
        CONFIG(release, debug|release){
            LIBS += -L$$PWD/../../_Modules/cpp/exiv2-0.24/msvc2012/exiv2lib/x64/ReleaseDLL -lexiv2
            LIBS += -LC:/Dev/qt/lib -lQt5Core -lQt5Gui -lQt5Concurrent -lQt5Widgets
        }
        else:CONFIG(debug, debug|release){
            LIBS += -L$$PWD/../../_Modules/cpp/exiv2-0.24/msvc2012/exiv2lib/x64/DebugDLL -lexiv2d
            LIBS += -LC:/Dev/qt/lib -lQt5Cored -lQt5Guid -lQt5Concurrentd -lQt5Widgetsd
        }
    }
    INCLUDEPATH += C:/Dev/qt/include
    DEPENDPATH += C:/Dev/qt/include
    INCLUDEPATH += $$PWD/../../_Modules/cpp/exiv2-0.24/msvc2012/include
    DEPENDPATH += $$PWD/../../_Modules/cpp/exiv2-0.24/msvc2012/include
}

#message($$INCLUDEPATH)
#message($$LIBS)
#message($$CONFIG)
#message($$QMAKE_CC)
#message($$QMAKE_LEX)
#message($$QMAKE_LEXFLAGS)
#message($$QMAKE_YACC)
#message($$QMAKE_YACCFLAGS)
#message($$QMAKE_CFLAG)
#message($$QMAKE_CFLAGS_DEPS)
#message($$QMAKE_CFLAGS_WARN_ON)
#message($$QMAKE_CFLAGS_WARN_OFF)
#message($$QMAKE_CFLAGS_RELEASE)
#message($$QMAKE_CFLAGS_DEBUG)
#message($$QMAKE_CXXFLAGS_DEBUG)
#message($$QMAKE_CFLAGS_YACC)
#message($$QMAKE_LFLAGS)

OTHER_FILES += \
    metadata.txt
