#ifndef IMAGEENTITYMODEL_H
#define IMAGEENTITYMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QMap>
#include <QMutex>
#include "imageentity.h"
#include <QDebug>
#include <QMargins>
#include <QPainter>
#include <memory>

class ImageEntityModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ImageEntityModel(QObject *parent = 0);
    void insertItem(const int row, ImageEntity& entity);
    void appendItem(ImageEntity& entity);
    void appendItems(const QStringList &newItems);
    void removeItem(const int row);
    void removeItem(ImageEntity *entity);
    ImageEntity *findItemByName(const QString name) const;
    int indexOf(ImageEntity *entity) const;
    void clear();
    QList<ImageEntity*> getItems() const;
    QList<ImageEntity*> getSelectedItems() {
        return selectedItems;
    }
    ImageEntity *getItem(int index) const;
    void setSelectedItems(QModelIndexList indexes);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    void cacheThumbnails(const QList<int> &indices) const;
    //void setThumbnail(const QModelIndex &index, std::shared_ptr<QImage> img) const;
    std::shared_ptr<QImage> getThumbnail(const QModelIndex &index) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

    QMargins & TileMargins() const { return const_cast<QMargins&>(m_TileMargins); }
    QMargins & TilePaddings() const { return const_cast<QMargins&>(m_TilePaddings); }
    QSize & TileSize() const { return const_cast<QSize&>(m_TileSize); }
    QSize & TotalTileSize() const { return const_cast<QSize&>(m_TotalTileSize); }
    void setTileMargins(int left, int top, int right, int bottom) { m_TileMargins = QMargins(left,top,right,bottom); UpdateTotalTileSize(); }
    void setTileMargins(QMargins tileMargins) { m_TileMargins = tileMargins; UpdateTotalTileSize(); }
    void setTilePaddings(int left, int top, int right, int bottom) { m_TilePaddings = QMargins(left,top,right,bottom); UpdateTotalTileSize(); }
    void setTilePaddings(QMargins tilePaddings) { m_TilePaddings = tilePaddings; UpdateTotalTileSize(); }
    void setTileSize(int width, int height) { m_TileSize = QSize(width,height); UpdateTotalTileSize(); }
    void setTileSize(QSize tileSize) { m_TileSize = tileSize; UpdateTotalTileSize(); }

    QTextOption & TextOption() const { return const_cast<QTextOption&>(m_TextOption); }
    void setTextOption(const QTextOption textOption) { m_TextOption = textOption; }

    void UpdateTotalTileSize() {
        m_TotalTileSize = QSize(m_TileMargins.left() + m_TilePaddings.left() + m_TileSize.width() + m_TilePaddings.right() + m_TileMargins.right(),
                                m_TileMargins.top() + m_TilePaddings.top() + m_TileSize.height() + m_TilePaddings.bottom() + m_TileMargins.bottom());
                               }
    QList<int> *cachedIndices = new QList<int>();
signals:

public slots:

private:
    QList<ImageEntity*> items;
    QList<ImageEntity*> selectedItems;
    QMap<QString,ImageEntity*> _items;

    QTextOption m_TextOption;
    QMargins m_TileMargins = QMargins(2,2,2,2);
    QMargins m_TilePaddings = QMargins(5,5,5,32);
    QSize m_TileSize = QSize(256,256);
    QSize m_TotalTileSize = QSize(270,297);
};

#endif // IMAGEENTITYMODEL_H
