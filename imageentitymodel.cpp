#include "imageentitymodel.h"
#include "io.h"

ImageEntityModel::ImageEntityModel(QObject *parent) :
    QAbstractListModel(parent)
{
    m_TextOption = QTextOption();
    m_TextOption.setAlignment(Qt::AlignCenter);
    m_TextOption.setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
}

void ImageEntityModel::insertItem(const int row, ImageEntity& entity)
{
    beginInsertRows(QModelIndex(),row,row);
    items.insert(row, &entity);
    _items.insert(entity.Filename(), &entity);
    endInsertRows();
}

void ImageEntityModel::appendItem(ImageEntity &entity)
{
    int row = items.count();
    insertItem(row, entity);
}

void ImageEntityModel::appendItems(const QStringList &newItems)
{
    int row = items.count();
    beginInsertRows(QModelIndex(),row,row+newItems.count());
    foreach (QString file,newItems)
    {
        ImageEntity* entity = new ImageEntity(file);
        //IO::ReadMeta(entity);
        items.append(entity);
        _items.insert(file,entity);
    }
    endInsertRows();
}

void ImageEntityModel::removeItem(const int row)
{
    beginRemoveRows(QModelIndex(),row,row);
    items.removeAt(row);
    if (_items.keys().length()<row)
    {
        _items.remove(_items.keys()[row]);
    }
    endRemoveRows();
}

void ImageEntityModel::removeItem(ImageEntity* entity)
{
    int row = items.indexOf(entity);
    beginRemoveRows(QModelIndex(),row,row);
    items.removeOne(entity);
    _items.remove(entity->Filename());
    endRemoveRows();
}

int ImageEntityModel::indexOf(ImageEntity* entity) const
{
    return items.indexOf(entity);
}

ImageEntity* ImageEntityModel::findItemByName(const QString name) const
{
    return _items.find(name).value();
    //return const_cast<ImageEntity&>(_items[name]);
}

void ImageEntityModel::clear()
{
    beginResetModel();
    items.clear();
    _items.clear();
    selectedItems.clear();
    endResetModel();
}

QList<ImageEntity*> ImageEntityModel::getItems() const{
    return items;
    //return _items.values();
}

ImageEntity* ImageEntityModel::getItem(int index) const{
    return items.at(index);
}

void ImageEntityModel::setSelectedItems(QModelIndexList indexes)
{
    selectedItems.clear();
    QModelIndexList::iterator index;
    for (index = indexes.begin(); index != indexes.end(); ++index)
    {
        selectedItems.append(items.at((*index).row()));
    }
}

int ImageEntityModel::rowCount(const QModelIndex &/*parent*/) const
{
    return items.count();
    //return _items.count();
}

QVariant ImageEntityModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || role != Qt::DisplayRole)
        return QVariant();
    return QVariant::fromValue(*items.at(index.row()));
    //return QVariant::fromValue(_items[_items.keys()[index.row()]]);
}

bool ImageEntityModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(index);
    Q_UNUSED(value);
    Q_UNUSED(role);
    /*if (index.isValid() && role == Qt::EditRole)
    {
        ImageEntity* entity = (&value.value<ImageEntity>());
        items.replace(index.row(), entity);
        _items[entity->Filename()] = entity;
        emit dataChanged(index,index);
        return true;
    }*/
    return false;
}

QMutex mutex;
QMap<QString,std::shared_ptr<QImage> > thumbnails;
void ImageEntityModel::cacheThumbnails(const QList<int>& indices) const
{
    cachedIndices->clear();
    QList<QString> cachedKeys = thumbnails.keys();

    QList<int>::const_iterator index;
    for (index = indices.begin(); index!= indices.end(); ++index)
    {
        //if (*index < 0) throw new IndexOutOfRangeException(string.Format("Index from Indices is less then 0: {0}", index));
        ImageEntity* entity = items[*index];
        if (!thumbnails.contains(entity->Filename()))
        {
            std::shared_ptr<QImage> img = IO::Thumbnails::ReadThumbnail(*entity);//QImage();
            if (!img->isNull())
            {
                //qDebug()<<"cached"<<entity->Filename();
                thumbnails.insert(entity->Filename(), img);
                cachedIndices->append(*index);
            }
            //else qDebug()<<"not cached"<<entity->Filename();
        }
        else
            cachedIndices->append(*index);
    }

    QList<QString>::iterator key;
    for (key = cachedKeys.begin(); key != cachedKeys.end(); ++key)
    {
        int idx = items.indexOf(this->findItemByName(*key));
        if (!indices.contains(idx))
        {
            thumbnails.remove(*key);
            //qDebug()<<"removed from cache"<<*key;
        }
    }
}

/*void ImageEntityModel::setThumbnail(const QModelIndex &index, std::shared_ptr<QImage> img) const
{
    ImageEntity* entity = items.at(index.row());
    qDebug()<<"setThumbnail"<<entity->Filename();
    //mutex.lock();
    //thumbnails.insert(entity.Filename(), img);
    //mutex.unlock();
}*/

std::shared_ptr<QImage> ImageEntityModel::getThumbnail(const QModelIndex &index) const
{
    //ImageEntity entity = items.at(index.row());
    ImageEntity* entity = items.at(index.row());
    //mutex.lock();
    std::shared_ptr<QImage> img;
    if (!thumbnails.isEmpty() && thumbnails.contains(entity->Filename()))
    {
        img = thumbnails.value(entity->Filename());
        //qDebug()<<"getThumbnail(from cache)"<<entity.Filename();
    }
    else if (entity->ThumbnailPos()!=0)
    {
        /*img = IO::Thumbnails::ReadThumbnail(*entity);//QImage();
        if (img->isNull())
        {
            qDebug()<<"getThumbnail(from file, pos eq 0)"<<entity->Filename();
            return img;
        }*/
        //thumbnails.insert(entity.Filename(), img);
        qDebug()<<"getThumbnail(from file)"<<entity->Filename();
    }
    else
    {
        qDebug()<<"getThumbnail(not in cache, pos eq 0)"<<entity->Filename();
    }
    //mutex.unlock();
    return img;
}

Qt::ItemFlags ImageEntityModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) return 0;
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}
