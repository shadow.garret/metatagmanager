#ifndef IMAGEENTITYDELEGATE_H
#define IMAGEENTITYDELEGATE_H

#include <QAbstractItemDelegate>
#include <QPainter>
#include <QMargins>

class ImageEntityDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    explicit ImageEntityDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
signals:

public slots:
private:
};

#endif // IMAGEENTITYDELEGATE_H
