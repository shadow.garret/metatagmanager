#include "imageentity.h"
#include <QDebug>

ImageEntity::ImageEntity(): filename(""), title(""), author(""), source(""), description(""), keywords(), size(), modified(),thumbnailPos(0)
{

}

ImageEntity::ImageEntity(const QString &file): ImageEntity::ImageEntity()
{
   this->setFilename(file);
}

// Copy method
ImageEntity::ImageEntity(const ImageEntity &other): ImageEntity::ImageEntity()
{
    this->setFilename(other.Filename());
    this->setTitle(other.Title());
    this->setAuthor(other.Author());
    this->setSource(other.Source());
    this->setDescription(other.Description());
    this->setKeywords(other.Keywords());
    this->setSize(other.Size());
    this->setModified(other.Modified());
    this->setThumbnailPos(other.ThumbnailPos());
    //thumbnail = other.thumbnail;
}

QDataStream& operator<<(QDataStream &out, const ImageEntity &entity)
{
    out<<(quint16)0xF00F;
    out<<entity.Filename();
    out<<entity.Title();
    out<<entity.Author();
    out<<entity.Source();
    out<<entity.Description();
    out<<entity.Keywords();
    out<<entity.Size();
    out<<entity.Modified();
    out<<entity.ThumbnailPos();
    out<<(quint16)0xF00F;
    return out;
}

QDataStream& operator>>(QDataStream &in, ImageEntity &entity)
{
    quint16 start;
    quint16 end;
    in>>start;
    if (start == (quint16)0xF00F)
    {
        QString filename,title,author,source,description;
        QList<QString> keywords;
        QSize size;
        QDateTime modified;
        quint64 thumbnailPos;
        in>>filename;
        in>>title;
        in>>author;
        in>>source;
        in>>description;
        in>>keywords;
        in>>size;
        in>>modified;
        in>>thumbnailPos;
        in>>end;
        entity.setFilename(filename);
        entity.setTitle(title);
        entity.setAuthor(author);
        entity.setSource(source);
        entity.setDescription(description);
        entity.setKeywords(keywords);
        entity.setSize(size);
        entity.setModified(modified);
        entity.setThumbnailPos(thumbnailPos);
    }
    else
    {
        qDebug()<<"ImageEntity>>: invalid start pos" <<(in.device()->pos()-4);
        //TODO: handle wrong position or invalid file
    }
    return in;
}

QString ImageEntity::toString()
{
    return QString("{Filename:%1, Title:%2, Author:%3, Source:%4, Size:%5}").arg(filename).arg(title).arg(author).arg(source).arg(QString("{%1x%2}").arg(size.height()).arg(size.width()));
}

/*void ImageEntity::setThumbnail(const QImage &img)
{
    thumbnail = img;
}*/
