#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScrollArea>
#include <QVBoxLayout>
#include "QDir"
#include <QFutureWatcher>
#include <QDebug>
#include <QtConcurrent>
#include <QDesktopServices>
#include "io.h"
#include "tileviewwidget.h"
#include "imageentitymodel.h"
#include "imageentitydelegate.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QString& getCurrentDirectory() const { return const_cast<QString&>(currentDirectory); }
    //QList<ImageEntity>& items() const {return const_cast<QList<ImageEntity>&>(Items); }

private:
    Ui::MainWindow *ui;
    void resizeEvent(QResizeEvent *);
    //QScrollArea *scrollarea;
    //TileViewWidget *tileview;
    //static QSize defaultThumbnailSize;
    //QSize thumbnailSize;
    QString currentDirectory;
    QStringList files;
    ImageEntityModel *model;
    ImageEntityDelegate *itemDelegate;
    //QList<ImageEntity> Items;
    QFutureWatcher<std::shared_ptr<QImage> > *thumbnailCreator;
    QFutureWatcher<void> *metaReader;

    QAction *openEntityAct;
    QAction *openEntityFolderAct;

private slots:
    void openFolder();
    void thumbnailReady(int num);
    void metaReady(int num);
    void currentChanged(const QModelIndex &current, const QModelIndex &previous);
    void doubleClicked(const QModelIndex &index);
    void tileviewContextMenuEvent(QContextMenuEvent* event);
    void openEntity();
    void openEntityFolder();
};

#endif // MAINWINDOW_H
