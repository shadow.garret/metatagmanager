#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>

/*
 * Xmp.dc.source        XmpText
 * Xmp.dc.description   LangAlt
 * Xmp.pdf.Author       XmpText
 * Xmp.pdf.Keywords     XmpText
 * Xmp.pdf.Title        XmpText
 */


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->actionOpen_Folder->setShortcut(QKeySequence::Open);
    connect(ui->actionOpen_Folder, SIGNAL(triggered()), this, SLOT(openFolder()));

    openEntityAct = new QAction(tr("Open"), ui->tileview);
    connect(openEntityAct, SIGNAL(triggered()), this, SLOT(openEntity()));
    openEntityFolderAct = new QAction(tr("Show in Folder"), ui->tileview);
    connect(openEntityFolderAct, SIGNAL(triggered()), this, SLOT(openEntityFolder()));

    model = new ImageEntityModel(this);
    model->setTileSize(256,256);
    itemDelegate = new ImageEntityDelegate(this);
    ui->tileview->setModel(model);
    ui->tileview->setItemDelegate(itemDelegate);
    connect(ui->tileview, SIGNAL(doubleClicked(QModelIndex)), SLOT(doubleClicked(QModelIndex)));
    connect(ui->tileview, SIGNAL(ContextMenuEvent(QContextMenuEvent*)), SLOT(tileviewContextMenuEvent(QContextMenuEvent*)));

    thumbnailCreator = new QFutureWatcher<std::shared_ptr<QImage> >(this);
    connect(thumbnailCreator, SIGNAL(resultReadyAt(int)), SLOT(thumbnailReady(int)));

    metaReader = new QFutureWatcher<void>(this);
    connect(metaReader, SIGNAL(resultReadyAt(int)), SLOT(metaReady(int)));
    connect(ui->tileview, SIGNAL(CurrentChanged(QModelIndex,QModelIndex)), SLOT(currentChanged(QModelIndex,QModelIndex)));


    //qDebug()<<QDir::currentPath();
}

void MainWindow::openFolder()
{
    QString folder = QFileDialog::getExistingDirectory(this);
    if (folder.length()>0 && folder != currentDirectory)
    {
        currentDirectory = folder;
        //IO::setDirectory(currentDirectory);
        QDir::setCurrent(currentDirectory);
        qDebug()<<"openFolder"<<folder;
        //QDir dir = QDir(folder);
        QDir dir = QDir::current();

        QStringList extensions; //({"*.png","*.jpg","*.jpeg","*.gif"})
        extensions << "*.png" << "*.jpg" << "*.jpeg" << "*.gif";
        files = dir.entryList(extensions, QDir::Files);
        model->clear();
        model->appendItems(files);
        IO::Thumbnails::setFile(*model);
        QList<ImageEntity*> items = model->getItems();
        QList<ImageEntity*> unavailableThumbnails;
        QList<ImageEntity*> unavailableMedatada;
        QList<ImageEntity*>::iterator entity;
        for (entity = items.begin(); entity != items.end(); ++entity)
        {
            if ((*entity)->ThumbnailPos()==0)
                unavailableThumbnails.append(*entity);
            if ((*entity)->Modified().isNull())
                unavailableMedatada.append(*entity);
        }
        qDebug()<<"unavailableThumbnails"<<unavailableThumbnails.count()<<"unavailableMetadata"<<unavailableMedatada.count();
        if (unavailableThumbnails.count()>0)
        {
            thumbnailCreator->setFuture(QtConcurrent::mapped(unavailableThumbnails,IO::Thumbnails::CreateThumbnail));
        }
        if (unavailableMedatada.count()>0)
        {
            //metaReader->setFuture(QtConcurrent::mapped(unavailableMedatada,IO::Meta::ReadMetaFromFile));
        }
        //metaReader->setFuture(QtConcurrent::mapped(model->getItems(), IO::ReadMetaFromFile));
        //TODO: create/read thumbnails for viewport
        //thumbnailCreator->setFuture(QtConcurrent::mapped(files,IO::Thumbnails::CreateThumbnail));
        qDebug()<<"files"<<files.count()<<"items"<<items.count();
        //metaReader->setFuture(QtConcurrent::mapped(model->children(),IO::ReadMeta));
    }
    else
        qDebug()<<"openFolder canceled";
}

void MainWindow::thumbnailReady(int num)
{
    ImageEntity* entity = model->getItem(num); //(model->index(num).data().value<ImageEntity*>());
    //std::shared_ptr<ImageEntity> entity = model->index(num).data().value<std::shared_ptr<ImageEntity> >();
    std::shared_ptr<QImage> img = thumbnailCreator->resultAt(num);
    //entity.setThumbnail(img);
    //model->setThumbnail(model->index(num), img);
    IO::Thumbnails::WriteThumbnail(entity, img);
    ui->tileview->update(model->index(num));
    //ui->tileview->viewport()->update();
    qDebug()<<"thumbnailReady"<<num<<files[num];
}

void MainWindow::metaReady(int num)
{
    Q_UNUSED(num)
    //ImageEntity* entity = (ImageEntity*)metaReader->resultAt(num);
    //qDebug()<<"metaReady"<<num<<""<<entity->toString();
    //model->setData(model->index(model->indexOf(entity)), QVariant::fromValue<ImageEntity>(entity),Qt::EditRole);
    //QVariant data = model->index(num).data();
    //data.setValue(metaReader->resultAt(num));
    //ImageEntity entity = model->index(num).data().value<ImageEntity>();

    //ImageEntity entity = metaReader->resultAt(num);
}

void MainWindow::currentChanged(const QModelIndex &current, const QModelIndex &/*previous*/)
{
    if (current.isValid())
    {
        ui->infopanel->setImageEntity(current.data().value<ImageEntity>());
    }
}

void MainWindow::doubleClicked(const QModelIndex &index)
{
    if(!index.isValid()) return;
    openEntity();//((ImageEntityModel*)index.model())->getItem(index.row())

}

void MainWindow::tileviewContextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(ui->tileview);
    menu.addAction(openEntityAct);
    menu.addAction(openEntityFolderAct);
    menu.exec(event->globalPos());
}

void MainWindow::openEntity()
{
    QUrl url = QUrl::fromLocalFile(QDir::current().absoluteFilePath(model->getSelectedItems().first()->Filename()));
    bool result = QDesktopServices::openUrl(url);
    qDebug()<<"open"<<url<<result;
}

void MainWindow::openEntityFolder()
{
    QStringList files;
    QList<ImageEntity*> selectedItems = model->getSelectedItems();
    QList<ImageEntity*>::iterator entity;
    for (entity = selectedItems.begin(); entity != selectedItems.end(); ++entity)
    {
        files.append(QDir::toNativeSeparators(QDir::current().absoluteFilePath((*entity)->Filename())));
    }

#if defined(Q_OS_WIN)
    files.insert(0, QLatin1String("/select"));
    QProcess::startDetached("explorer.exe", QStringList(files.join(QString(","))));
#elif defined(Q_OS_MAC)

#endif
}


MainWindow::~MainWindow()
{
    thumbnailCreator->cancel();
    metaReader->cancel();
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    //scrollarea->setGeometry(0,0,ui->centralwidget->width(),ui->centralwidget->height());
    //tileview->UpdateLayout();
    ui->tileview->UpdateLayout();
}
