#ifndef TILEVIEWWIDGET_H
#define TILEVIEWWIDGET_H

//#include <QWidget>
#include <QAbstractItemView>
#include <QPainter>
#include <QMargins>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QScrollBar>
#include <math.h>
#include <QVariant>
#include <QMenu>
#include "imageentitymodel.h"

namespace Ui {
    class TileViewWidget;
}

class TileViewWidget : public QAbstractItemView //QWidget //
{
    Q_OBJECT

public:
    void setItems(const int newCount) { m_Items = newCount; UpdateLayout(); }
    void setModel(QAbstractItemModel *model);

    int& Items() const { return const_cast<int&>(m_Items); }

    void UpdateLayout();
    QPoint GetItemLocation(const int index) const;
    int GetIndexAtLocation(const QPoint point) const;
    QList<int> GetVisibleIndices(QRect rect);
    explicit TileViewWidget(QWidget *parent = 0);
    ~TileViewWidget();

signals:
    void CurrentChanged(const QModelIndex &current, const QModelIndex &previous);
    void ContextMenuEvent(QContextMenuEvent *event);

private:
    int m_Items;
    int m_ItemsPerRow = 1;
    int heightForWidth(int width) const;
    QSize sizeHint() const;
    QSize minimumSizeHint() const;
    QTextOption textOption;
    Ui::TileViewWidget *ui;

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *);
    //void mousePressEvent (QMouseEvent *event);
    //QAbstractItemView
    QRect visualRect(const QModelIndex &index) const;
    void scrollTo(const QModelIndex &index, ScrollHint hint);
    QModelIndex indexAt(const QPoint &point) const;
    QModelIndex moveCursor(CursorAction cursorAction, Qt::KeyboardModifiers modifiers);
    int horizontalOffset() const;
    int verticalOffset() const;
    bool isIndexHidden(const QModelIndex &/*index*/) const { return false; }
    void setSelection(const QRect &rect, QItemSelectionModel::SelectionFlags flags);
    QRegion visualRegionForSelection(const QItemSelection &selection) const;

protected slots:
    void rowsInserted(const QModelIndex &parent, int start, int end);
    void rowsAboutToBeRemoved(const QModelIndex &parent, int start, int end);
    void currentChanged(const QModelIndex &current, const QModelIndex &previous);
    void contextMenuEvent(QContextMenuEvent *event);
};

#endif // TILEVIEWWIDGET_H
