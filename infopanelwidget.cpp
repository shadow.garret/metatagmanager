#include "infopanelwidget.h"
#include "ui_infopanel.h"
#include <QDebug>

InfoPanelWidget::InfoPanelWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InfoPanelWidget)
{
    ui->setupUi(this);
    ui->keywordsListWidget->setSortingEnabled(false);
    ui->keywordsListWidget->addItem(defaultItem = createDefaultItem());
    connect(ui->keywordsListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(editKeyword(QListWidgetItem*)));
    connect(ui->keywordsListWidget, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(keywordChanged(QListWidgetItem*)));
}

void InfoPanelWidget::setImageEntity(const ImageEntity &entity)
{
    currentEntity = &entity;
    ui->filenameLineEdit->clear();
    ui->titleLineEdit->clear();
    ui->authorLineEdit->clear();
    ui->sourceLineEdit->clear();
    ui->keywordsListWidget->clear();
    ui->descriptionTextEdit->clear();

    if (currentEntity != NULL)
    {
        ui->filenameLineEdit->setText(currentEntity->Filename());
        ui->sizeValueLabel->setText(QString("%1x%2").arg(entity.Size().width()).arg(entity.Size().height()));
        ui->titleLineEdit->setText(currentEntity->Title());
        ui->authorLineEdit->setText(currentEntity->Author());
        ui->sourceLineEdit->setText(currentEntity->Source());
        ui->descriptionTextEdit->setText(currentEntity->Description());

        QList<QString>::const_iterator keyword;
        for (keyword = currentEntity->Keywords().begin(); keyword != currentEntity->Keywords().end(); ++keyword)
        {
            QListWidgetItem *item = new QListWidgetItem(*keyword, ui->keywordsListWidget);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui->keywordsListWidget->addItem(item);
        }
        ui->keywordsListWidget->addItem(defaultItem = createDefaultItem());
    }
}

QListWidgetItem* InfoPanelWidget::createDefaultItem()
{
    QListWidgetItem* item = new QListWidgetItem(tr("New"));
    item->setFlags(item->flags() | Qt::ItemIsEditable);
    item->setTextColor(Qt::gray);
    return item;
}

void InfoPanelWidget::editKeyword(QListWidgetItem *item)
{
    if (item == defaultItem)
    {
        QListWidgetItem* newItem = new QListWidgetItem();
        newItem->setFlags(newItem->flags() | Qt::ItemIsEditable);
        ui->keywordsListWidget->insertItem(ui->keywordsListWidget->row(defaultItem), newItem);
        ui->keywordsListWidget->editItem(newItem);
        qDebug()<<ui->keywordsListWidget->row(newItem)<<ui->keywordsListWidget->row(defaultItem);
    }
    else
        ui->keywordsListWidget->editItem(item);
}

void InfoPanelWidget::keywordChanged(QListWidgetItem *item)
{
}

InfoPanelWidget::~InfoPanelWidget()
{
    delete ui;
}
