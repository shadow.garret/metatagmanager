#ifndef IO_H
#define IO_H

#include <exiv2/exiv2.hpp>
#include <exiv2/error.hpp>
#include <exiv2/xmp.hpp>
#include <QString>
#include <QImage>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QDataStream>
#include <QBuffer>
#include <memory>
#include "imageentity.h"
#include "imageentitymodel.h"


class IO
{
public:
    //IO();
    class Meta
    {
    public:
        static ImageEntity *ReadMetaFromFile(ImageEntity *entity);
        static void WriteMetaToFile(ImageEntity& entity);
    private:
        static std::shared_ptr<QFile> filePtr;
    };

    class Thumbnails
    {
    public:
        static void setFile(ImageEntityModel &model);
        static std::shared_ptr<QImage> CreateThumbnail(ImageEntity *entity);
        static std::shared_ptr<QImage> ReadThumbnail(ImageEntity &entity);
        static void WriteThumbnail(ImageEntity *entity, std::shared_ptr<QImage> img);
    private:
        static std::shared_ptr<QFile> filePtr;
        ~Thumbnails();
    };
};

#endif // IO_H
