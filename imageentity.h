#ifndef IMAGEENTITY_H
#define IMAGEENTITY_H

#include <QString>
#include <QSize>
#include <QDateTime>
#include <QImage>
#include <QDataStream>

/*struct ImageEntity {
    QString Filename;

    QString Title;
    QString Author;
    QString Source;
    QString Description;
    QList<QString> Keywords;

    long thumbnailPos;
    QDateTime modified;
};*/

class ImageEntity
{
public:
    ImageEntity();
    ImageEntity(const QString &file);
    ImageEntity(const ImageEntity &other);

    QString &Filename() const { return const_cast<QString&>(filename); }
    QString &Title() const { return const_cast<QString&>(title); }
    QString &Author() const { return const_cast<QString&>(author); }
    QString &Source() const { return const_cast<QString&>(source); }
    QString &Description() const { return const_cast<QString&>(description); }
    QList<QString> &Keywords() const { return const_cast<QList<QString>&>(keywords); }
    QSize &Size() const { return const_cast<QSize&>(size); }
    QDateTime &Modified() const { return const_cast<QDateTime&>(modified); }
    quint64 &ThumbnailPos() const { return const_cast<quint64&>(thumbnailPos); }

    //QImage &Thumbnail() {return thumbnail;}
    bool operator == (const ImageEntity &a) const { return this->filename == a.filename; }

    void setFilename(const QString &value) { filename = value; }
    void setTitle(const QString &value) { title = value; }
    void setAuthor(const QString &value) { author = value; }
    void setSource(const QString &value) { source = value; }
    void setDescription(const QString &value) { description = value; }
    void setKeywords(const QList<QString> &value) { keywords = value; }
    void setSize(const QSize &value) { size = value; }
    void setModified(const QDateTime &value) { modified = value; }
    void setThumbnailPos(const quint64& value) { thumbnailPos = value; }

    //void setThumbnail(const QImage &img); //temp

    QString toString();
signals:

public slots:

private:
    QString filename;
    QString title;
    QString author;
    QString source;
    QString description;
    QList<QString> keywords;
    QSize size;
    QDateTime modified;
    quint64 thumbnailPos;
    //QImage thumbnail; //temp

};

Q_DECLARE_METATYPE(ImageEntity)
#endif // IMAGEENTITY_H
