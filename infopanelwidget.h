#ifndef INFOPANEL_H
#define INFOPANEL_H

#include <QWidget>
#include <QListWidgetItem>
#include "imageentity.h"

namespace Ui {
class InfoPanelWidget;
}

class InfoPanelWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InfoPanelWidget(QWidget *parent = 0);
    void setImageEntity(const ImageEntity &entity);
    void setImageEntities(const QList<ImageEntity> &entities);
    ~InfoPanelWidget();

public slots:
    void editKeyword(QListWidgetItem* item);
    void keywordChanged(QListWidgetItem* item);

private:
    QListWidgetItem* createDefaultItem();
    Ui::InfoPanelWidget *ui;
    const ImageEntity* currentEntity;
    QListWidgetItem* defaultItem;
};

#endif // INFOPANEL_H
